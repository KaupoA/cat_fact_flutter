import 'dart:convert';

import 'package:catfactflutter/model/cat_fact.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

abstract class CatFactRepository {
  Future<List<All>> refreshCatFacts();
}

class CatFactRepositoryImpl implements CatFactRepository {

  final String tableName = 'cat_fact';

  @override
  Future<http.Response> fetchCatFacts() {
    return http.get('https://cat-fact.herokuapp.com/facts');
  }

  Future<List<All>> refreshCatFacts() async {
    try {
      final response = await fetchCatFacts();

      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        var data = json.decode(response.body);
        List<All> list = CatFact.fromJson(data).all;
        await deleteCatFacts();
        for (All all in list) {
          await insertCatFact(all);
        }
        return list;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to load CatFacts');
      }
    } catch (e) {
      return await catFacts();
    }
  }

  // Open the database and store the reference.
  Future<Database> database() async {
    return openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), 'cat_facts_database.db'),

      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        return db.execute(
          "CREATE TABLE $tableName(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, text TEXT)",
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );
  }

  Future<void> insertCatFact(All all) async {
    // Get a reference to the database.
    final Database db = await database();

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      tableName,
      all.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<All>> catFacts() async {
    // Get a reference to the database.
    final Database db = await database();

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query(tableName);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      return All(
        id: maps[i]['id'],
        text: maps[i]['text'],
      );
    });
  }

  Future<void> deleteCatFacts() async {
    // Get a reference to the database.
    final db = await database();

    // Remove the Dog from the Database.
    await db.delete(tableName);
  }
}
