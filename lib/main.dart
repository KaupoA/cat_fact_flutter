import 'package:catfactflutter/bloc/bloc.dart';
import 'package:catfactflutter/repository/cat_fact_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'model/cat_fact.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  CatFactRepository catFactRepository;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fetch Data Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
        builder: (context) => CatFactsBloc(catFactRepository: CatFactRepositoryImpl()),
        appBar: AppBar(
          title: Text('Fetch Data Example'),
        ),
        body: Center(
          child: FutureBuilder<List<All>>(
            future: catFactRepository.refreshCatFacts(),
            builder: (context, catFacts) {
              if (catFacts.hasData) {
                return ListView.builder(
                  itemCount: catFacts.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                        title: Text(catFacts.data[index].text),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SingleCatFact(
                                    all: catFacts.data[index],
                                  )));
                        });
                  },
                );
              } else if (catFacts.hasError) {
                return Text("${catFacts.error}");
              }

              // By default, show a loading spinner.
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}

class SingleCatFact extends StatelessWidget {
  final All all;

  const SingleCatFact({Key key, this.all}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cat Fact'),
      ),
      body: Center(
        child: Text(all.text),
      ),
    );
  }
}
