import 'package:equatable/equatable.dart';

class CatFact extends Equatable{
  List<All> all;

  CatFact({this.all});

  CatFact.fromJson(Map<String, dynamic> json) {
    if (json['all'] != null) {
      all = new List<All>();
      json['all'].forEach((v) {
        all.add(new All.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.all != null) {
      data['all'] = this.all.map((v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  // TODO: implement props
  List<Object> get props => [all];
}

class All {
  int id;
  String text;

  All({this.id, this.text});

  All.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    text = json['text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.id;
    data['text'] = this.text;
    return data;
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'text': text,
    };
  }

  factory All.fromDatabase(Map json) {
    return All(
      id: int.parse(json['id']),
      text: json['text'],
    );
  }
}