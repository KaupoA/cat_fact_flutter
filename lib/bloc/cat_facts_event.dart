import 'package:equatable/equatable.dart';

abstract class CatFactsEvent extends Equatable {}

class FetchCatFacts extends CatFactsEvent {

  @override
  List<Object> get props => [];
}