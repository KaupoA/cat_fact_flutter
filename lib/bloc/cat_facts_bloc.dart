import 'package:catfactflutter/model/cat_fact.dart';
import 'package:catfactflutter/repository/cat_fact_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'cat_facts_event.dart';
import 'cat_facts_state.dart';

class CatFactsBloc extends Bloc<CatFactsEvent, CatFactsState> {
  final CatFactRepository catFactRepository;

  CatFactsBloc({@required this.catFactRepository});

  @override
  CatFactsState get initialState => CatFactsInitialState();

  @override
  Stream<CatFactsState> mapEventToState(CatFactsEvent event,) async* {
    if (event is FetchCatFacts) {
      yield CatFactsLoadingState();
      try {
        List<All> all = await catFactRepository.refreshCatFacts();
        yield CatFactsLoadedState(all: all);
      } catch (e) {
        yield CatFactsErrorState(message: e.toString());
      }
    }
  }
}
