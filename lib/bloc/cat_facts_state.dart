import 'package:catfactflutter/model/cat_fact.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class CatFactsState extends Equatable {}

class CatFactsInitialState extends CatFactsState {
  @override
  List<Object> get props => [];
}

class CatFactsLoadingState extends CatFactsState {
  @override
  List<Object> get props => [];
}

class CatFactsLoadedState extends CatFactsState {
  List<All> all;

  CatFactsLoadedState({@required this.all});

  @override
  List<Object> get props => [];
}

class CatFactsErrorState extends CatFactsState {
  final String message;

  CatFactsErrorState({@required this.message});

  @override
  List<Object> get props => [];
}